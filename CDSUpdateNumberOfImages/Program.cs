﻿using System;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace CDSUpdateNumberOfImages
{
	class Program
	{
		public static string ConnectionStringDataCDS = @"Data Source=10.17.220.55;Initial Catalog=DBCDSContent;User ID=central;Password=Cen@tral";
		public static readonly string ENV = "Production";

		static void Main(string[] args)
		{
			Console.WriteLine("Start CDS Update Number Of Images Fix");
			Console.WriteLine($"Env : {ENV}");
			Console.WriteLine($"DateTime run : {DateTime.Now}");
			Console.WriteLine($"Delay for start program 3 second");
			Thread.Sleep(3000);

			using (SqlConnection connection = new SqlConnection(ConnectionStringDataCDS))
			{
				string sourcePath = string.Empty;

				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = @"SELECT * FROM TBProduct P 
								INNER JOIN TBProductApproved PA ON(P.PID = PA.PID)
								where NumOfImage = 0 
								and P.Status not in (0,2,4,5) and (P.IsFirstStockGR = 1 or P.Status in (6,9)) and PA.Status = 2 
								and EffectiveDate <= getdate() and getdate() < dateadd(day,1,ExpiredDate) and p.IsFree = 0";
					DataTable productImages = new DataTable();
					using (SqlDataAdapter adt = new SqlDataAdapter(command))
					{
						adt.Fill(productImages);
					}
					int pidCount = productImages.Rows.Count;
					int iCount = 0;
					int progressValue = 0;
					foreach (DataRow row in productImages.Rows)
					{
						iCount++;
						progressValue = ((iCount * 100) / pidCount);
						string pId = (string)row["PID"];
						Console.WriteLine("Update Number of Image PID : {0}", pId);
						string imageLargePath = "http://static0.central.co.th/productimages/tpimage/{0}";

						int picIndex = 1;
						string pidImageName = picIndex == 1 ? pId + ".jpg" : string.Format("{0}_x{1}.jpg", pId, picIndex);
						string largePath = string.Format(imageLargePath, pidImageName);

						int numOfImage = 0;

						while (ImageExist(largePath))
						{
							numOfImage++;
							picIndex++;
							pidImageName = picIndex == 1 ? pId + ".jpg" : string.Format("{0}_x{1}.jpg", pId, picIndex);
							largePath = string.Format(imageLargePath, pidImageName);
						}

						UpdateNumberOfImage(pId, numOfImage);
						Console.WriteLine("Progress : {0}%", progressValue);
					}
				}
			}
		}

		private static bool ImageExist(string linkUrl)
		{
			try
			{
				HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(linkUrl);
				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
					return response.StatusCode == HttpStatusCode.OK && IsValidImage(response.ContentType);
				}
			}
			catch
			{
			}
			return false;
		}

		private static bool IsValidImage(string contentType)
		{
			if (contentType.ToLower() != "image/jpg" &&
						contentType.ToLower() != "image/jpeg" &&
						contentType.ToLower() != "image/pjpeg" &&
						contentType.ToLower() != "image/gif" &&
						contentType.ToLower() != "image/x-png" &&
						contentType.ToLower() != "image/png")
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		private static bool UpdateNumberOfImage(string pID, int numOfImage)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionStringDataCDS))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = @"update TBProduct SET NumOfImage=@NumOfImage
											WHERE PID=@PID";
					command.Parameters.AddWithValue("@PID", pID);
					command.Parameters.AddWithValue("@NumOfImage", numOfImage);

					return command.ExecuteNonQuery() > 0;
				}
			}
		}

	}
}
